namespace Makerswebapp.Models
{
    public class ResponseOb
    {
         public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Result { get; set; }
    }
}
public class REGISTROS_FACTURACIONResponse
{
	public string ciclo { get; set; }

	public string departamento { get; set; }

	public string municipio { get; set; }

	public string sector { get; set; }

	public string anio { get; set; }
	
	public string mes { get; set; }

	public string codCuenta { get; set; }

	public string digitosChequeo { get; set; }

	public string codMedidor { get; set; }

	public string prefijoMedidor { get; set; }

	public string lecturaAnterior { get; set; }

	public string lecturaTomada { get; set; }

	public string anomaliaTomada { get; set; }

	public string comentarioTomado { get; set; }

	public string fechaHora { get; set; }

	public string codLector { get; set; }

	public string rutaAnteriorLeida { get; set; }

	public string tiempoLectura { get; set; }

	public string nombreArchivo { get; set; }

	public string coordenadax { get; set; }

	public string coordenaday { get; set; }

	public string satelites { get; set; }

	public string distanciaCuentas { get; set; }

	public string fechaSatelite { get; set; }

	public string altura { get; set; }

	public string codTerminal { get; set; }

	public string indicadorNovedad { get; set; }

	public string indicadorCodigoBarras { get; set; }

	public string intentos { get; set; }

	public string codCriticaTpl { get; set; }

	public string totalFacturado { get; set; }

	public string consumoFactura { get; set; }

	public string subsidioContribucion { get; set; }

	public string consumoActiva { get; set; }

	public string consumoReactiva { get; set; }

	public string consumoCT { get; set; }

	public string numeroConceptos { get; set; }

	public string indicadorFacturacion { get; set; }

	public string numeroFacturas { get; set; }

	public string fechaVencimiento { get; set; }

	public string fechaCorte { get; set; }

	public string lecturaModificada { get; set; }


	public string digitos { get; set; }

	public string procesado { get; set; }

	public string codCriticaSiec { get; set; }

	public string codNovedad { get; set; }
}
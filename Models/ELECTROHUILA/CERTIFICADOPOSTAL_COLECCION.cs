public class CERTIFICADOPOSTAL_COLECCION
{
	public string ciclo { get; set; }

	public string ano { get; set; }

	public string mes { get; set; }

	public string cuenta { get; set; }

	public string nombreUsuario { get; set; }

	public string direccion { get; set; }

	public string fecha { get; set; }

	public string hora { get; set; }

	public string telefono { get; set; }

	public string recibe { get; set; }

	public string lector { get; set; }

	public string cedula { get; set; }

	public string enviado { get; set; }
}
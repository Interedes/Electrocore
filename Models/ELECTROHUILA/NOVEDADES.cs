public class NOVEDADES
{
	public string numeroActa1 { get; set; }

	public string ciclo1 { get; set; }

	public string codigoObservacion1 { get; set; }

	public string fecha1 { get; set; }

	public string cuenta1 { get; set; }

	public string DigitosChequeo1 { get; set; }

	public string nombreUsuario1 { get; set; }

	public string cedula1 { get; set; }

	public string direccion1 { get; set; }

	public string barrio1 { get; set; }

	public string telefono1 { get; set; }

	public string rutaAnterior1 { get; set; }

	public string rutaActual1 { get; set; }

	public string rutaPropuesta1 { get; set; }

	public string rutaPosterior1 { get; set; }

	public string marca1 { get; set; }

	public string numero1 { get; set; }

	public string conexion1 { get; set; }

	public string modelo1 { get; set; }

	public string constante1 { get; set; }

	public string voltaje1 { get; set; }

	public string corriente1 { get; set; }

	public string tipo1 { get; set; }

	public string lectura1 { get; set; }

	public string selloTapa1 { get; set; }

	public string selloBornera1 { get; set; }

	public string selloGabinete1 { get; set; }

	public string claseServicio1 { get; set; }

	public string comentario1 { get; set; }

	public string comentario21 { get; set; }

	public string lector1 { get; set; }

	public string mes1 { get; set; }

	public string anio1 { get; set; }

	public string causaNovedad1 { get; set; }

	public string tipoNovedad1 { get; set; }

	public string desCausalNovedad1 { get; set; }

	public string estado1 { get; set; }

}
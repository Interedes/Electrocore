public class AFOROS
{
	public string numeroActa1 { get; set; }

	public string codigoAforo1 { get; set; }

	public string cantidad1 { get; set; }

	public string w1 { get; set; }

	public string tipoConexion1 { get; set; }

	public string lector1 { get; set; }

	public string codigoCuenta1 { get; set; }

	public string ciclo1 { get; set; }

	public string descripcionAforo1 { get; set; }

	public string mes1 { get; set; }

	public string anio1 { get; set; }
}
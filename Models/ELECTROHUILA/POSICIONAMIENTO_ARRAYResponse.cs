public class POSICIONAMIENTO_ARRAYResponse
{
    public string serialTerminal { get; set; }

	public string codCuenta { get; set; }

	public string fechaHora { get; set; }

	public string coordenadax { get; set; }

	public string coordenaday { get; set; }

	public string numeroSatelites { get; set; }

	public string fechaSatelite { get; set; }
}
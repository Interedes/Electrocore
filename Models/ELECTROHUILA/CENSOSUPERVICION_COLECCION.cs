public class CENSOSUPERVICION_COLECCION
{
	public string ciclo { get; set; }

	public string ano { get; set; }

	public string mes { get; set; }

	public string cuenta { get; set; }

	public string nombreUsuario { get; set; }

	public string direccion { get; set; }

	public string fecha { get; set; }

	public string hora { get; set; }

	public string telefono { get; set; }

	public string barrio { get; set; }

	public string lector { get; set; }

	public string contador { get; set; }

	public string lectura { get; set; }


	public string entregasi { get; set; }

	public string dificilsi { get; set; }

	public string facturasi { get; set; }

	public string etiquetasi { get; set; }

	public string dotacionsi { get; set; }

	public string codlectorcomercial { get; set; }

	public string enviado { get; set; }

	public string observacion { get; set; }

	public string longitud { get; set; }

	public string latitud { get; set; }
}

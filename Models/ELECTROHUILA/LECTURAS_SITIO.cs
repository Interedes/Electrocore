
using datamakerslib.Repository;

namespace Electrocore.Models.ELECTROHUILA{

	public class LECTURAS_SITIO:IEntityBase<long>
		{
			public long Id {get;set;}
			public string anio1 { get; set; }

			public string mes1 { get; set; }

			public string codigoCuenta1 { get; set; }

			public string digitosChequeo1 { get; set; }

			public string numeroMedidor1 { get; set; }

			public string lectura1 { get; set; }

			public string causa1 { get; set; }

			public string digitos1 { get; set; }

			public string fechaHora1 { get; set; }

			public string lector1 { get; set; }

			public string procesado1 { get; set; }

			public string distancia1 { get; set; }
}
}

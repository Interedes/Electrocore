public class AFOROS_COLECCIONResponse
{
	public string numeroActa { get; set; }

	public string codigoAforo { get; set; }

	public string cantidad { get; set; }

	public string w { get; set; }

	public string tipoConexion { get; set; }

	public string lector { get; set; }

	public string codigoCuenta { get; set; }

	public string ciclo { get; set; }

	public string descripcionAforo { get; set; }

	public string mes { get; set; }

	public string anio { get; set; }
}
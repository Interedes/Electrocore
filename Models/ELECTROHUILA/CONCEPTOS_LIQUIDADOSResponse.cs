public class CONCEPTOS_LIQUIDADOSResponse
{
	public string ciclo { get; set; }

	public string anio { get; set; }

	public string mes { get; set; }

	public string codCuenta { get; set; }

	public string digitosChequeo { get; set; }

	public string coConcepto { get; set; }

	public string indicadorActividad { get; set; }

	public string valor { get; set; }

	public string IdConcepto { get; set; }
}
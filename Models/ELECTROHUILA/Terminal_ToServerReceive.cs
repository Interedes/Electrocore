public class Terminal_ToServerReceive
{
	public string rutaArchivo { get; set; }

	public double tamanoArchivo {get; set; }

	public byte[] cadenaBytes {get; set; }

	public int Comprimido {get; set; }

	public string DirectorioDondeDescomprimir {get; set;}

}
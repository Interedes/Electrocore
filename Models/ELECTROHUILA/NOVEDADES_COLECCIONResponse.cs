public class NOVEDADES_COLECCIONResponse
{
	public string numeroActa { get; set; }

	public string ciclo { get; set; }

	public string codigoObservacion { get; set; }

	public string fecha { get; set; }

	public string cuenta { get; set; }

	public string DigitosChequeo { get; set; }

	public string nombreUsuario { get; set; }

	public string cedula { get; set; }

	public string direccion { get; set; }

	public string barrio { get; set; }

	public string telefono { get; set; }

	public string rutaAnterior { get; set; }

	public string rutaActual { get; set; }

	public string rutaPropuesta { get; set; }

	public string rutaPosterior { get; set; }

	public string marca { get; set; }

	public string numero { get; set; }

	public string conexion { get; set; }

	public string modelo { get; set; }

	public string constante { get; set; }

	public string voltaje { get; set; }

	public string corriente { get; set; }

	public string tipo { get; set; }

	public string lectura { get; set; }

	public string selloTapa { get; set; }

	public string selloBornera { get; set; }

	public string selloGabinete { get; set; }

	public string claseServicio { get; set; }

	public string comentario { get; set; }

	public string comentario2 { get; set; }

	public string lector { get; set; }

	public string mes { get; set; }

	public string anio { get; set; }

	public string causaNovedad { get; set; }

	public string tipoNovedad { get; set; }

	public string desCausalNovedad { get; set; }

	public string estado { get; set; }
}
public class LECTURAS_SITIOResponse
{
	public string anio { get; set; }

	public string mes { get; set; }

	public string codigoCuenta { get; set; }

	public string digitosChequeo { get; set; }

	public string numeroMedidor { get; set; }

	public string lectura { get; set; }

	public string causa { get; set; }

	public string digitos { get; set; }

	public string fechaHora { get; set; }

	public string lector { get; set; }

	public string procesado { get; set; }

	public string distancia { get; set; }
}
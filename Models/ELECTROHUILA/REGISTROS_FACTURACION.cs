public class REGISTROS_FACTURACION
{
	public string ciclo1 { get; set; }

	public string departamento1 { get; set; }

	public string municipio1 { get; set; }

	public string sector1 { get; set; }

	public string anio1 { get; set; }
	
	public string mes1 { get; set; }

	public string codCuenta1 { get; set; }

	public string digitosChequeo1 { get; set; }

	public string codMedidor1 { get; set; }

	public string prefijoMedidor1 { get; set; }

	public string lecturaAnterior1 { get; set; }

	public string lecturaTomada1 { get; set; }

	public string anomaliaTomada1 { get; set; }

	public string comentarioTomado1 { get; set; }

	public string fechaHora1 { get; set; }

	public string codLector1 { get; set; }

	public string rutaAnteriorLeida1 { get; set; }

	public string tiempoLectura1 { get; set; }

	public string nombreArchivo1 { get; set; }

	public string coordenadax1 { get; set; }

	public string coordenaday1 { get; set; }

	public string satelites1 { get; set; }

	public string distanciaCuentas1 { get; set; }

	public string fechaSatelite1 { get; set; }

	public string altura1 { get; set; }

	public string codTerminal1 { get; set; }

	public string indicadorNovedad1 { get; set; }

	public string indicadorCodigoBarras1 { get; set; }

	public string intentos1 { get; set; }

	public string codCriticaTpl1 { get; set; }

	public string totalFacturado1 { get; set; }

	public string consumoFactura1 { get; set; }

	public string subsidioContribucion1 { get; set; }

	public string consumoActiva1 { get; set; }

	public string consumoReactiva1 { get; set; }

	public string consumoCT1 { get; set; }

	public string numeroConceptos1 { get; set; }

	public string indicadorFacturacion1 { get; set; }

	public string numeroFacturas1 { get; set; }

	public string fechaVencimiento1 { get; set; }

	public string fechaCorte1 { get; set; }

	public string lecturaModificada1 { get; set; }

	public string lecturaModificada2 { get; set; }

	public string digitos1 { get; set; }

	public string procesado1 { get; set; }

	public string codCriticaSiec1 { get; set; }

	public string codNovedad1 { get; set; }

}
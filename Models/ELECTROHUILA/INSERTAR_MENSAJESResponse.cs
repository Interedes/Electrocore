public class INSERTAR_MENSAJESResponse 
{
	public string remitente { get; set; }

	public string receptor { get; set; }

	public string mensaje { get; set; }

	public string estado { get; set; }

	public string fechaMensaje { get; set; }
}
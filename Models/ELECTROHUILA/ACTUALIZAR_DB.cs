public class ACTUALIZAR_DB
{
    public string ciclo1 { get; set; }

    public string municipio1 { get; set; }

    public string sesion1 { get; set; }

    public string zona1 { get; set; }

    public string anio1 { get; set; }

    public string mes1 { get; set; }

    public string cod_cuenta1 { get; set; }

    public string prefijo_medidor1 { get; set; }

    public string cod_medidor1 { get; set; }

    public string lectura_tomada1 { get; set; }

    public string anomalia_tomada1 { get; set; }

    public string descrip_anomalia1 { get; set; }

    public string comentario_tomado1 { get; set; }

    public string descrip_comentario1 { get; set; }

    public string distancia_medidor1 { get; set; }

    public string fecha_hora1 { get; set; }

    public string cod_lector1 { get; set; }

    public string ruta_anterior_leida1 { get; set; }

    public string tiempo_lectura1 { get; set; }

    public string modelo_ciclo1 { get; set; }

    public string nombre_archivo1 { get; set; }

    public string coordenadax1 { get; set; }

    public string coordenaday1 { get; set; }

    public string satelites1 { get; set; }

    public string distancia_cuentas1 { get; set; }

    public string fecha_satelite1 { get; set; }

    public string altura1 { get; set; }

    public string cod_terminal1 { get; set; }

    public string indicador_novedad1 { get; set; }

    public string indicador_codigo_barras1 { get; set; }

    public string intentos1 { get; set; }

    public string cod_critica_tpl1 { get; set; }

    public string total_facturado1 { get; set; }

    public string consumo_factura1 { get; set; }

    public string subsidio_contribucion1 { get; set; }    
}
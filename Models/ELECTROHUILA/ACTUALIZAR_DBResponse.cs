public class ACTUALIZAR_DBResponse
{
	public string ciclo { get; set; }

    public string municipio { get; set; }

    public string sesion { get; set; }

    public string zona { get; set; }

    public string anio { get; set; }

    public string mes { get; set; }

    public string cod_cuenta { get; set; }

    public string prefijo_medidor { get; set; }

    public string cod_medidor { get; set; }

    public string lectura_tomada { get; set; }

    public string anomalia_tomada { get; set; }

    public string descrip_anomalia { get; set; }

    public string comentario_tomado { get; set; }

    public string descrip_comentario { get; set; }

    public string distancia_medidor { get; set; }

    public string fecha_hora { get; set; }

    public string cod_lector { get; set; }

    public string ruta_anterior_leida { get; set; }

    public string tiempo_lectura { get; set; }

    public string modelo_ciclo { get; set; }

    public string nombre_archivo { get; set; }

    public string coordenadax { get; set; }

    public string coordenaday { get; set; }

    public string satelites { get; set; }

    public string distancia_cuentas { get; set; }

    public string fecha_satelite { get; set; }

    public string altura { get; set; }

    public string cod_terminal { get; set; }

    public string indicador_novedad { get; set; }

    public string indicador_codigo_barras { get; set; }

    public string intentos { get; set; }

    public string cod_critica_tpl { get; set; }

    public string total_facturado { get; set; }

    public string consumo_factura { get; set; }

    public string subsidio_contribucion { get; set; } 
}
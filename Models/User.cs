﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using datamakerslib.Repository;

namespace Electrocore.Models
{
  /// [Table("public.Users")]
    public class User:IEntityBase<long>
    {   
        [Key]
        public long Id { get; set; }
        ///Puede ser null
       // public int? Workshop_Id { get; set; }
        
       // public int UserTypeId { get;  set;  }
       // [JsonProperty("NOMBRE_CLIENTE")]
        public string Nombre { get; set; }

       // [JsonProperty("CODIGO_CLIENTE")]
       // public string Codigo_Cliente { get; set; }

        //[JsonProperty("NUMERO_CEDULA")]
        public string Numero_Cedula { get; set; }
        

       // [JsonProperty("DIRECCION")]
        public string Direccion { get; set; }

       // [JsonProperty("CIUDAD")]
        public string Ciudad { get; set; }

       // [JsonProperty("TELF_FIJO")]
       // public string Telf_Fijo { get; set; }

       // [JsonProperty("TELF_MOVIL")]
        public string Telf_Movil { get; set; }

        //[JsonProperty("DIRECCION_ELECTRONICA")]
        public string Direccion_Electronica { get; set; }

        //[JsonProperty("FECHA_NACIMIENTO")]
        public string Fecha_Nacimiento { get; set; }

        //[JsonProperty("SEXO")]
       // public string Sexo { get; set; }

        public bool Is_Admin { get; set; }

        public string Password { get; set; }

              //[JsonIgnore] //Propiedad para relacion dinamica
       // [JsonIgnore]
        //public virtual ICollection<Notification> Notifications { get; set; }

       
      
        //[JsonProperty("ClienteExisteResult")]
      //  public string ClienteExisteResult { get; set; }

       // public virtual UserType usertypo{get;set;}
       
    }
}
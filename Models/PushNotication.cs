﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Makerswebapp.Models
{
    public class PushNotification
    {
        public List<string> Tags { get; set; }

        public string TipeNotification { get; set; }
        
        public string ContentNotification { get; set; }
        
    }
}
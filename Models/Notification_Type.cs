﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using datamakerslib.Repository;

namespace Electrocore.Models
{
    public class Notification_Type:IEntityBase<int>
    {

        [Key]
        public int Id { get; set; }

        [Display(Name = "Nombre Tipo Notificacion")]
        public string Name_Notification_Type { get; set; }

        [Display(Name = "Sigla Tipo Notificacion")]
        public string Sigla { get; set; }

        [JsonIgnore]
        public IList<Notification> Notifications { get; set; }
    }
}

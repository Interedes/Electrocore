using System.ComponentModel.DataAnnotations;
using datamakerslib.Repository;

namespace  Electrocore.Models
{
    public class UserType:IEntityBase<int>
    {
         [Key]   
         public int Id{get;set;}
         public string nameUserType{get;set;}
        
    }
}
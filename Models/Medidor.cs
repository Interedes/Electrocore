using System.Collections.Generic;
using datamakerslib.Repository;
using Newtonsoft.Json;

namespace Electrocore.Models
{
    public class Medidor:IEntityBase<long>
    {
       public long Id{get;set;} 
       public string Codigo{get;set;}
       public string Marca{get;set;}
        
       [JsonIgnore] 
       public virtual ICollection<Lectura> lecturas{get;set;}
        
    }
}
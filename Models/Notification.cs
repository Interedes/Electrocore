﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Electrocore.Models;
using datamakerslib.Repository;

namespace Electrocore.Models
{
    public class Notification:IEntityBase<long>
    {
         [Key]
        public long Id { get; set; }
      
        public int User_Id { get; set; }

        public int Notification_Type_Id { get; set; }

        public string Content_Notification { get; set; }
        
        public DateTime Fecha { get; set; }

        public string Hora { get; set; }

        public string Numero_Cedula { get; set; }

        public string Param_Notification { get; set; }

        //Relaciond de muchos a 1 (1 cita tiene un  horario)
        [JsonIgnore]//Propiedad para relacion dinamica
        public virtual Notification_Type Notification_Type { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

    }
}

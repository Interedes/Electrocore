using System.Threading.Tasks;
using Electrocore.Models;
using Electrocore.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Electrocore.Controllers
{
    [Route("api/[controller]")]
    public class MedidorController:Controller
    {
        
        private readonly IMedidorRepository _IMedidorRepository;

        private readonly IUserRepository _IuserRepository;
        public MedidorController(IUserRepository userRepository, IMedidorRepository IMedidorRepository)
        {
             _IuserRepository = userRepository;
             _IMedidorRepository=IMedidorRepository;
         }
        
        [HttpGet]
        public async Task<IActionResult> Get()
        {
                
           var lectura= await _IMedidorRepository.GetAllAsync();//AllIncludingAsync(c=>c.Medidor);//GetAllAsync();
        
            return Ok(lectura);
        }
         [HttpGet("{id}")]
        public async Task<Medidor> Get(int id)
        {
            return await this._IMedidorRepository.GetSingleAsync(id);//.GetProduct(id);
        }
        [HttpPost]
        public async Task<Medidor> Post([FromBody]Medidor medidor)
        {
             await this._IMedidorRepository.AddAsync(medidor);
             await _IMedidorRepository.Commit();

             return medidor;
        }
        [HttpPut("{id}")]
        public async Task<Medidor> Put(long id, [FromBody]Medidor medidor)
        {
            var medidoredit= await _IMedidorRepository.GetSingleAsync(m=>m.Id==id);
            if(medidoredit!=null){
               // medidoredit.Id=medidor.Id;
                medidoredit.Codigo=medidor.Codigo;
                medidoredit.Marca=medidor.Marca;
                
               // medidoredit.Id=id;
                
                await this._IMedidorRepository.EditAsync(medidoredit);
                }
            return medidor;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
          var deleteEntity= await _IMedidorRepository.GetSingleAsync(id);
          if(deleteEntity!=null){
                 this._IMedidorRepository.Delete(deleteEntity);
                 await _IMedidorRepository.Commit();

          }
         return Ok(deleteEntity);
           
        }
    }
}
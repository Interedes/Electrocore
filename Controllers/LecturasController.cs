using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Electrocore.Models;
using Electrocore.Repository;
using Electrocore.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Electrocore.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class LecturasController:Controller
    {
        private readonly IlecturasRepository _IlecturasRespository;

        private readonly IUserRepository _IuserRepository;
        private readonly IMapper _mapper;
        public LecturasController(IMapper mapper, IUserRepository userRepository, IlecturasRepository IlecturasRespository)
        {
             _IuserRepository = userRepository;
             _IlecturasRespository=IlecturasRespository;
             _mapper = mapper;
         }
        
        /// <summary>Gets  elements contained in the Lectura.</summary>
        /// <returns>The elements contained in the Lectura.</returns>
        //[Produces(typeof(Lectura))]      
        [HttpGet]
        [SwaggerResponse(200, typeof(Lectura))]
        public async Task<IActionResult> Get()
        {
                
           var lectura= await _IlecturasRespository.GetAllAsync();//AllIncludingAsync(c=>c.Medidor);//GetAllAsync();
         var model = _mapper.Map<IEnumerable<Lectura>, IEnumerable<LecturaViewModel>>(lectura);
            return Ok(model);
        }
         [HttpGet("{id}")]
        public async Task<Lectura> Get(int id)
        {
            return await this._IlecturasRespository.GetSingleAsync(id);//.GetProduct(id);
        }
        //[Produces("application/json", Type=typeof(value))]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Lectura lectura)
        {
            if(!ModelState.IsValid){

                return BadRequest(ModelState);
            }
             await this._IlecturasRespository.AddAsync(lectura);
             await _IlecturasRespository.Commit();

             return Ok(lectura);
        }
        [HttpPut("{id}")]
        public async Task<Lectura> Put(long id, [FromBody]Lectura lectura)
        {
            var lecturaedit= await _IlecturasRespository.GetSingleAsync(m=>m.Id==id);
            if(lecturaedit!=null){
             
                lecturaedit.Medidor_id=lectura.Medidor_id;
                lecturaedit.Periodo_id=lectura.Periodo_id;
                lecturaedit.Ruta_id=lectura.Ruta_id;
                lecturaedit.Servicio_id=lectura.Servicio_id;
                lecturaedit.Tipo_medida_id=lectura.Tipo_medida_id;
                lecturaedit.lectura=lectura.lectura;
               // lecturaedit.Id=id;
                
                await this._IlecturasRespository.EditAsync(lecturaedit);
                }
            return lectura;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
          var deleteEntity= await _IlecturasRespository.GetSingleAsync(id);
          if(deleteEntity!=null){
                 this._IlecturasRespository.Delete(deleteEntity);
                 await _IlecturasRespository.Commit();

          }
         return Ok(deleteEntity);
           
        }

        
    }
}
using Electrocore.Models;
using Electrocore.Models.ELECTROHUILA;
using datamakerslib.Repository;
namespace Electrocore.Repository
{
   
        
    public interface IUserRepository : IEntityBaseRepository<User,long> { }

    // public interface IUserTypeRepository: IEntityBaseRepository<UserType,int> { }

     public interface IlecturasRepository: IEntityBaseRepository<Lectura, long>{}

     public interface IMedidorRepository:IEntityBaseRepository<Medidor, long>{}
    
}
using datamakerslib.Repository;
using Electrocore.DataContext;
using Electrocore.Models;

namespace Electrocore.Repository
{
    public class UserTypeRepository:EntityBaseRepository<UserType,int, MyAppContext>// IUserTypeRepository
    {
        public UserTypeRepository(MyAppContext context)
            : base(context)
        { }
    
        
    }
}
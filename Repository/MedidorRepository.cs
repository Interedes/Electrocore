using datamakerslib.Repository;
using Electrocore.DataContext;
using Electrocore.Models;

namespace Electrocore.Repository
{
    public class MedidorRepository:EntityBaseRepository<Medidor,long, MyAppContext>, IMedidorRepository
    {
        public MedidorRepository(MyAppContext context)
            : base(context)
        { }
    
        
    }
}
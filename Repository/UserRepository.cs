using datamakerslib.Repository;
using Electrocore.DataContext;
using Electrocore.Models;

namespace Electrocore.Repository
{
    public class UserRepository:EntityBaseRepository<User,long, MyAppContext>, IUserRepository
    {
        public UserRepository(MyAppContext context)
            : base(context)
        { }
    
        
    }
}
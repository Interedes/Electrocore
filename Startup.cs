﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using datamakerslib.Startup;
using Electrocore.DataContext;
using Electrocore.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Electrocore.Options;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Electrocore.CustomTokenProvider;
using Blog.TokenAuthGettingStarted.CustomTokenProvider;
using Microsoft.AspNetCore.Authorization;
using System.Security.Cryptography;
using AspNet.Security.OAuth.Validation;
using AutoMapper;

namespace Electrocore
{
    public partial class Startup
    {
      
       RsaSecurityKey  key;
        public TokenOptions _tokenAuth;
        public Startup(IConfiguration configuration)
        {
          
            Configuration = configuration;   
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           
            
           // services.AddSingleton<TokenOptions>(_tokenAuth);
                 services
              .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });
               services.AddSwaggerGen(c =>
            {
                
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() { In = "header", Description = "Please insert JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                 
               // c.AddSecurityDefinition("Bearer", new ApiKeyScheme() { In = "header", Description = "Please insert JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Version = "v1",
                        Title = "ELECTROHUILA CORE app API",
                        Description = "A  API for app ELECTROHUILA",
                        TermsOfService = "Some terms ..."
                    }
                     

                );
             
            });

           
           services.AddLogging();
       //services.AddSwaggerGen();
        services.AddAutoMapper(); 
          //.AddApiExplorer();
             // Add service and create Policy with options 
           services.AddCors(options => { options.AddPolicy("CorsPolicy", 
                                      builder => builder.AllowAnyOrigin() 
                                                        .AllowAnyMethod() 
                                                        .AllowAnyHeader() 
                                                        .AllowCredentials()); 
                                  }); 

          //  services.AddScoped<IDataAccessProvider, DataAccessPostgreSqlProvider.DataAccessPostgreSqlProvider>();
             //configuracion de objetos
             services.AddOptions();
             services.AddDataAccess<MyAppContext>();
              var configurationSection = Configuration.GetSection("ConnectionStrings:DataAccessPostgreSqlProvider").Value;
           // var configureOptions = services.BuildServiceProvider().GetRequiredService<IConfigureOptions<ConnectionStrings>>();
             services.AddDbContext<MyAppContext>(options =>
             {
                options.UseOpenIddict();
                options.UseNpgsql(
                configurationSection,
                    b => b.MigrationsAssembly("Electrocore")
                );
            });
          
            
          services.AddOpenIddict(options =>
        {
            // Register the Entity Framework stores.
            options.AddEntityFrameworkCoreStores<MyAppContext>();
            // Register the ASP.NET Core MVC binder used by OpenIddict.
            // Note: if you don't call this method, you won't be able to
            // bind OpenIdConnectRequest or OpenIdConnectResponse parameters.
            options.AddMvcBinders();
            // Enable the token endpoint.
            options.EnableTokenEndpoint("/connect/token");
            // Enable the password flow.
            options.AllowPasswordFlow();
            // During development, you can disable the HTTPS requirement.
            options.DisableHttpsRequirement();
        });
        // Register the validation handler, that is used to decrypt the tokens.
        services.AddAuthentication(options =>
        {
            options.DefaultScheme = OAuthValidationDefaults.AuthenticationScheme;
        })
        .AddOAuthValidation();
             services.AddTransient<IUserRepository, UserRepository>(); 
           // services.AddTransient<IUserTypeRepository, UserTypeRepository>(); 
             services.AddTransient<IlecturasRepository, LecturasRepository>(); 
             services.AddTransient<IMedidorRepository, MedidorRepository>(); 
          
           
             services.Configure<Settings>(options =>
                {
                    
                    options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                    options.Database = Configuration.GetSection("MongoConnection:Database").Value;
                }); 

               


        
            
               services.AddMvc();     
             
        }
       
 
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
             app.UseAuthentication();
             app.UseCors("CorsPolicy"); 
             app.UseMvcWithDefaultRoute();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
    
   // app.UseStaticFiles();
            app.UseDeveloperExceptionPage();

           app.UseSwagger();
        
  
            app.UseSwaggerUI(c =>
            {
                
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Electro");
               // c.InjectOnCompleteJavaScript("/swagger-ui/basic-auth.js"); 
                 c.InjectOnCompleteJavaScript("/swagger-ui/authorization1.js");
               // c.InjectOnCompleteJavaScript("https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"); // https://cdnjs.com/libraries/crypto-js
                // c.InjectOnCompleteJavaScript("/swagger-ui/authorization2.js");
            });
            app.UseStaticFiles();
            app.Run(async (context) =>  context.Response.Redirect("/swagger"));
            // app.UseStaticFiles();
            
          
           
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

           // app.UseTokenProvider(_tokenProviderOptions);
           

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

           // app.UseMvc();
        }
    }
}

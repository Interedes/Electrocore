using System;
using Electrocore.Models;
using Microsoft.IdentityModel.Tokens;

namespace Electrocore.Options
{
    public interface ITokenProvider
    {
         string CreateToken(User user, DateTime expiry);

    // TokenValidationParameters is from Microsoft.IdentityModel.Tokens
    TokenValidationParameters GetValidationParameters();
    }
}
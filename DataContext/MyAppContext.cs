using datamakerslib.DataContext;
using Electrocore.Models;
using Microsoft.EntityFrameworkCore;

namespace Electrocore.DataContext
{
    public class MyAppContext: EntityContextBase<MyAppContext>
    {
        public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
        { }
        ///colocar las clases
        public DbSet<Lectura> Lecturas { get; set; }
        public DbSet<Medidor> medidor {get;set;}
        public DbSet<User> user{get;set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
               modelBuilder.Entity<Medidor>()
             .HasMany(e => e.lecturas)
            .WithOne(c => c.medidor).HasForeignKey(c=>c.Medidor_id);

             modelBuilder.Entity<Lectura>().HasKey(m => m.Id);
            modelBuilder.Entity<Medidor>().HasKey(m=>m.Id);
             modelBuilder.Entity<User>().HasKey(m=>m.Id);
            base.OnModelCreating(modelBuilder);
        }
    }
}
using AutoMapper;
using Electrocore.Models;
using Electrocore.ViewModels;

public class MappingProfile : Profile {
    public MappingProfile() {
        // Add as many of these lines as you need to map your objects
        CreateMap<Lectura, LecturaViewModel>();
        CreateMap<LecturaViewModel, Lectura>();
    }
}